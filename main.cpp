#include <iostream>
#include "Algorithms.h"


using namespace std;

unsigned long long N = 956;

int main() {

    Algorithm ZadanieAB(12,-1,-1, N);
    ZadanieAB.setAlgorithm(GaussaSeidla);
    ZadanieAB.run();
    ZadanieAB.saveAs("ZadanieB_MetodaGaussaSeidla.txt");

    ZadanieAB.setAlgorithm(Jacobi);
    ZadanieAB.run();
    ZadanieAB.saveAs("ZadanieB_MetodaJacobi.txt");
    Algorithm ZadanieCD(3, -1, -1, N);
    ZadanieCD.setAlgorithm(GaussaSeidla);
    ZadanieCD.run();
    ZadanieCD.saveAs("ZadanieC_MetodaGaussaSeidla.txt");

    ZadanieCD.setAlgorithm(Jacobi);
    ZadanieCD.run();
    ZadanieCD.saveAs("ZadanieC_MetodaJacobi.txt");
    ZadanieCD.setAlgorithm(LUfactorization);
    ZadanieCD.run();
    ZadanieCD.saveAs("ZadanieC_FaktoryzacjaLU.txt");

    unsigned long long tabN[] {100, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000};

    /*
    for (unsigned long long i : tabN) {
        Algorithm ZadanieE(12, -1, -1, i);
        ZadanieE.setAlgorithm(GaussaSeidla);
        ZadanieE.run();
        ZadanieE.saveAs(((string)("ZadanieE_MetodaGaussaSeidla_N" + to_string(i) + ".txt")).data());

        ZadanieE.setAlgorithm(Jacobi);
        ZadanieE.run();
        ZadanieE.saveAs(((string)("ZadanieE_MetodaJacobi_N" + to_string(i) + ".txt")).data());
    }

    for (unsigned long long i : tabN) {
        Algorithm ZadanieE(12, -1, -1, i);
        ZadanieE.setAlgorithm(LUfactorization);
        ZadanieE.run();
        ZadanieE.saveAs(((string)("ZadanieE_FaktoryzacjaLU_N" + to_string(i) + ".txt")).data());
    }
     */
    return 0;
}