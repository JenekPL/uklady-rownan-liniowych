//
// Created by Mateusz Jenek on 16.04.2018.
//

#ifndef NUMERKI_MARTIX_H
#define NUMERKI_MARTIX_H

#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <iomanip>
#include <sstream>

class Matrix {
private:
    unsigned int mX, mY;
    double** mMat;

public:
    Matrix(unsigned int x, unsigned int y) {
        this->mX = x;
        this->mY = y;

        this->mMat = new double*[mX];
        for(int x = 0; x < mX; x++)
            this->mMat[x] = new double[y];

        for (int x = 0; x < mX; x++) {
            for (int y = 0; y < mY; y++){
                this->mMat[x][y] = 0;
            }
        }
    }

    ~Matrix() {
        for(int x = 0; x < mX; x++) {
            delete[] mMat[x];
        }
        delete[] mMat;
    }

    unsigned int getWidth() {
        return mX;
    }

    unsigned int getHeight() {
        return mY;
    }

    double getValue(unsigned int x, unsigned int y) {
        return mMat[x][y];
    }

    Matrix* setValue(unsigned int x, unsigned int y, double value) {
        //if(x >= mX || y >= mY) return false;

        mMat[x][y] = value;
        return this;
    }

    bool equals(Matrix &matrix) {

        if(mX != matrix.getWidth() || mY != matrix.getHeight()) return false;

        for(int x = 0; x < mX; x++){
            for(int y = 0; y < mY; y++) {
                if(this->getValue(x,y) != matrix.getValue(x,y)) return false;
            }
        }

        return true;
    }

    Matrix* add(Matrix& matrix) {
        if(mX != matrix.getWidth() || mY != matrix.getHeight()) return this;

        for(int x = 0; x < mX; x++){
            for(int y = 0; y < mY; y++) {
                mMat[x][y] += matrix.getValue(x,y);
            }
        }
        return this;
    }

    Matrix* subtract(Matrix& matrix) {
        if(mX != matrix.getWidth() || mY != matrix.getHeight()) return this;

        for(int x = 0; x < mX; x++){
            for(int y = 0; y < mY; y++) {
                mMat[x][y] -= matrix.getValue(x,y);
            }
        }
        return this;
    }

    Matrix* dotProduct(Matrix& matrix) {
        if(mX != matrix.getWidth() || mY != matrix.getHeight()) return this;

        for(int x = 0; x < mX; x++){
            for(int y = 0; y < mY; y++) {
                mMat[x][y] *= matrix.getValue(x,y);
            }
        }
        return this;
    }

    Matrix* multiply(double a) {
        for(int x = 0; x < mX; x++){
            for(int y = 0; y < mY; y++) {
                mMat[x][y] *= a;
            }
        }
        return this;
    }

    std::string toString() {
        std::string result;
        for(unsigned int y = 0; y < mY; y++) {
            for(unsigned int x = 0; x < mX; x++) {
                std::ostringstream out;
                out << std::setprecision(9) << mMat[x][y];
                result += out.str();
                if(x == mX-1) result += "\n";
                else result += " ";
            }
        }
        return result;
    }

    static Matrix* getIndentityMatix(unsigned int x) {
        auto matrix = new Matrix(x, x);
        for(int i = 0; i < x; i++)
            matrix->setValue(i,i,1);
        return matrix;
    }

    static Matrix* getBandMatrix(unsigned int size, Matrix& vec) {
        auto matrix = new Matrix(size, size);
        if(vec.getWidth() > size) return matrix;
            for (int i = 0; i < vec.getWidth(); i++) {
            for(int j = 0; j < size-i; j++) {
                 matrix->setValue(j+i,j,vec.getValue(i, 0));
                matrix->setValue(j,j+i,vec.getValue(i, 0));
            }
        }
        return matrix;
    }

    static Matrix* multiply(Matrix& mat1, Matrix& mat2) {
        auto matrix = new Matrix(mat2.getWidth(), mat1.getHeight());
        if(mat1.getWidth() != mat2.getHeight()) return matrix;

        for(unsigned int i = 0; i < mat2.getWidth(); i++) {
            for(unsigned int j = 0; j < mat1.getHeight(); j++) {
                double cell = 0;
                for(unsigned int x = 0; x < mat1.getWidth(); x++) {
                    double a = mat1.getValue(x,j);
                    double b = mat2.getValue(i,x);
                    cell += mat1.getValue(x,j) * mat2.getValue(i,x);
                }
                matrix->setValue(i,j,cell);
            }
        }
        return matrix;
    }

    static Matrix* copy(Matrix& mat) {
        auto matrix = new Matrix(mat.getWidth(), mat.getHeight());
        for(unsigned int i = 0; i < mat.getWidth(); i++) {
            for (unsigned int j = 0; j < mat.getHeight(); j++) {
                matrix->setValue(i,j,mat.getValue(i,j));
            }
        }
        return matrix;
    }


    std::tuple<Matrix*, Matrix*> getLUMatrixes() {

        Matrix *U = copy(*this);
        Matrix *L = getIndentityMatix(mX);

        for (int k = 0; k < mX-1; k++) {
            for (int j = k+1; j < mX; j++) {
                L->setValue(k, j, U->getValue(k, j)/U->getValue(k,k));
                for (int x = k; x < mX; x++) {
                    U->setValue(x, j, U->getValue(x, j) - (L->getValue(k, j) * U->getValue(x, k)));
                }
            }
        }
        return std::make_tuple(L, U);
    }
};


#endif //NUMERKI_MARTIX_H
