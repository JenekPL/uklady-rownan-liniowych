//
// Created by Mateusz Jenek on 22.04.2018.
//

#ifndef NUMERKI_ALGORITHMS_H
#define NUMERKI_ALGORITHMS_H

#include <vector>
#include <cmath>
#include "Martix.h"
#include <chrono>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

enum Algorithms {
    NONE = 0,
    GaussaSeidla = 1,
    Jacobi = 2,
    LUfactorization = 3
};

class Algorithm {
private:

    struct Result {
        vector<double> res;
        Matrix *x = nullptr;
        int iterations = 0;
        double time = 0;

        Result(unsigned long long N) {
            x = new Matrix(1, N);
        }

        ~Result() {
            delete x;
        }

        double getLastResiduum() {
            if(res.size() > 0) return res.at(res.size()-1);
            else return 0;
        }

        void pushBackResiduum(double value) {
            res.push_back(value);
        }

        Matrix* getResAsMatrix() {
            Matrix *matrix = new Matrix(1, res.size());
            for(int i = 0; i < res.size(); i++)
                matrix->setValue(0,i,res.at(i));
            return matrix;
        }
    };

    unsigned long long m_N;
    Matrix *m_A;
    Matrix *m_b;
    Result *m_lastResult = nullptr;
    Algorithms m_algorithm = NONE;
    const static int MAX_ITERATION = 100;
    constexpr static double ACCURACY = 0.000000001;

public:

    Algorithm(double a1, double a2, double a3, unsigned long long N) {
        this->m_N = N;

        Matrix *a = (new Matrix(3,1))->setValue(0,0,a1)->setValue(1,0,a2)->setValue(2,0,a3);
        m_A = Matrix::getBandMatrix(N, *a);
        delete a;

        m_b = new Matrix(1, N);
        for (unsigned int n = 1; n <= N; n++) {
            m_b->setValue(0, n-1, sin(6*n));
        }
    }

    ~Algorithm(){
        delete m_b;
        delete m_A;
        delete m_lastResult;
    }

    void setAlgorithm(Algorithms x) {
        m_algorithm = x;
    }

    void run() {
        if (m_algorithm != NONE) {
            delete m_lastResult;
            m_lastResult = new Result(m_N);
        }

        auto start = chrono::system_clock::now();
        switch (m_algorithm) {
            case GaussaSeidla:
                GaussaSeidlaFunc();
                break;
            case Jacobi:
                JacobiFunc();
                break;
            case LUfactorization:
                LUfactorizationFunc();
                break;
            default:
                cout << "Choose Algorythm using method setAlgorithm" << endl;
                return;
        }
        auto end = chrono::system_clock::now();
        m_lastResult->time = ((chrono::duration<double>)(end-start)).count();

        cout << "Completed in time: " << m_lastResult->time << endl;
        if(m_lastResult->getLastResiduum() > ACCURACY) cout << "Max Iteration Occurred" << endl;
    }

    void printTime() { cout << "Time: " << m_lastResult->time << endl; }
    void printResiduum() { cout << "Residuum: " << endl <<m_lastResult->getResAsMatrix()->toString(); }
    void printIteration() { cout << "Iterations: " << m_lastResult->iterations << endl; }
    void printX() { cout << "X: " << endl << m_lastResult->x->toString(); }

    void saveAs(char* filename) {
        if(m_lastResult == nullptr) {
            cout << "Nothing to save, invoke run method first" << endl;
            return;
        }

        ofstream file;
        file.open(filename, ios::out | ios::trunc);
        if(!file.good()){
            cout << "Failed to save file" << endl;
        }
        else {
            file << "TIME: " << m_lastResult->time << endl
                 << "ITERATIONS:" << m_lastResult->iterations << endl
                 << "NORM_RESIDUUM: " << endl << m_lastResult->getResAsMatrix()->toString()
                 << "X: " << endl << m_lastResult->x->toString();
        }
        file.close();
    }

private:
    void addResiduumFromLastResult() {
        Matrix *res = Matrix::multiply(*m_A, *(m_lastResult->x))->subtract(*m_b);

        double tempRes = 0;
        for(int resi = 0; resi < res->getHeight(); resi++) {
            tempRes += res->getValue(0, resi) * res->getValue(0, resi);
        }
        m_lastResult->pushBackResiduum(sqrt(tempRes));
        delete res;
    }

    void GaussaSeidlaFunc() {
        do {
            m_lastResult->iterations++;

            for (unsigned int i = 0; i < m_N; i++) {
                double value = m_b->getValue(0, i);
                for (unsigned int j = 0; j < m_N; j++) {
                    if (j != i) {
                        value -= m_A->getValue(i, j) * m_lastResult->x->getValue(0, j);
                    }
                }
                value /= m_A->getValue(i, i);
                m_lastResult->x->setValue(0, i, value);
            }

            addResiduumFromLastResult();
        } while(m_lastResult->getLastResiduum() > ACCURACY && m_lastResult->iterations < MAX_ITERATION);
    }

    void JacobiFunc() {
        do {
            m_lastResult->iterations++;
            auto *newX = new Matrix(1, m_N);

            for (int i = 0; i < m_N; i++) {
                double value = m_b->getValue(0, i);
                for (int j = 0; j < m_N; j++) {
                    if (j != i) {
                        value -= m_A->getValue(i, j) * m_lastResult->x->getValue(0, j);
                    }
                }
                value /= m_A->getValue(i, i);
                newX->setValue(0, i, value);
            }
            addResiduumFromLastResult();
            delete m_lastResult->x;
            m_lastResult->x = newX;

        } while( m_lastResult->getLastResiduum() > ACCURACY &&  m_lastResult->iterations < MAX_ITERATION);
    }

    void LUfactorizationFunc() {
        auto luMatrixes = m_A->getLUMatrixes();
        Matrix *L = get<0>(luMatrixes);
        Matrix *U = get<1>(luMatrixes);
        Matrix *y = new Matrix(1, L->getHeight());

        for (int i = 0; i < L->getHeight(); i++){
            double value = m_b->getValue(0,i);
            for(int j = 0; j < i; j++) {
                value -= L->getValue(j, i) * y->getValue(0,j);
            }
            y->setValue(0, i, value/L->getValue(i,i));
        }

        m_lastResult->x = new Matrix(1, L->getHeight());
        for (int i = U->getHeight()-1; i >= 0 ; i--){
            double value = y->getValue(0,i);
            for(int j = U->getHeight()-1; j > i; j--) {
                value -= U->getValue(j, i) * m_lastResult->x->getValue(0,j);
            }
            m_lastResult->x->setValue(0, i, value/U->getValue(i,i));
        }

        addResiduumFromLastResult();
        delete L;
        delete U;
        delete y;
    }

};

#endif //NUMERKI_ALGORITHMS_H
